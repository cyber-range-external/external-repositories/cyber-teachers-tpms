networkname=cybernet



id=`sudo docker container ls -a | grep tpms | sed 's/ .*//'`

if [ "x$id" != "x" ]; then
    echo -n "Docker container tpms already exists, should I kill the old container [y/N]: "
    read ret
    if [ "x$ret" == "xy" ] || [ "x$ret" == "xY" ]; then
	echo "removing container"
	sudo docker rm -f $id
    elif [ "x$ret" == "xn" ] || [ "x$ret" == "xN" ]; then
	echo -n ""
    elif [ "x$ret" == "x" ]; then
	 exit
    else
	echo "answer not recognized ... exiting"
    fi
fi

sudo docker run -d --name=tpms --privileged -v /dev/bus/usb:/dev/bus/usb -v $PWD/working:/home/tpms/working --net="host" -i -t tpms 


id=`sudo docker container ls -a | grep tpms | wc -l`


if [ "x$id" == "x0" ]; then
    echo "ERROR: container did not start"
    exit
fi

mkdir -p ~/.ssh
touch ~/.ssh/config
num=`cat ~/.ssh/config | grep tpms | wc -l`

if [ "x$num" == "x0" ]; then
    echo "Adding ssh config for tpms"
    echo "Host tpms" >> ~/.ssh/config
    echo "     HostName 127.0.0.1" >> ~/.ssh/config
    echo "     User tpms" >> ~/.ssh/config
    echo "     Port 1022" >> ~/.ssh/config
fi

echo "Container created"
echo "To log in to the container run 'ssh -X tpms'"


