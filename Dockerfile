FROM ubuntu:xenial

# Update apt
RUN apt-get update --fix-missing && apt-get install -y \
    apt-utils \
    iputils-ping sudo net-tools htop \
    ssh openssh-server \
    git gitg \
    emacs gedit \
    udev \
    bash-completion usbutils \
    python-apt python-pip \
    x11-apps

# Install pybombs with pip
RUN pip install --upgrade pip
RUN pip install pybombs

# Create new user and set password
RUN useradd -ms /bin/bash tpms
RUN echo 'tpms:tpms' | chpasswd
RUN echo 'root:tpms' | chpasswd
RUN usermod -aG sudo tpms
RUN echo "tpms ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/tpms
RUN chown -R tpms:tpms /home/tpms 



# Build GNU Radio
USER tpms
WORKDIR /home/tpms
RUN pybombs recipes add gr-recipes git+https://github.com/gnuradio/gr-recipes.git
RUN mkdir prefix
RUN pybombs prefix init /home/tpms/prefix -a prefix
RUN pybombs config makewidth 2
# Fix the gr-iio.lwr to point to master
RUN sed -i 's/gitrev.*/gitbranch: master/' /home/tpms/.pybombs/recipes/gr-recipes/gr-iio.lwr

RUN sed -i 's/gitbranch.*/gitbranch: release_003_010_003_000/' /home/tpms/.pybombs/recipes/gr-recipes/uhd.lwr

RUN sed -i 's/gitbranch.*/gitbranch: v3.7.11/' /home/tpms/.pybombs/recipes/gr-recipes/gnuradio.lwr

# This will install gnuradio with limesdr and plutosdr support
RUN pybombs install uhd
RUN pybombs install gnuradio
RUN pybombs install soapysdr limesuite gr-osmosdr gr-iio

# Run ldconfig so that the builds below can access the libraries

USER root
RUN ldconfig

# Get the custom gr-tpms blocks and install
USER tpms
WORKDIR /home/tpms
RUN git clone https://code.vt.edu/cyber-teachers/gr-tpms.git
WORKDIR /home/tpms/gr-tpms
RUN mkdir build
WORKDIR /home/tpms/gr-tpms/build
RUN /bin/bash -c "source ~/prefix/setup_env.sh && cmake .."
RUN make
RUN make install


# Get ready in case a B210 is used
USER tpms
WORKDIR /home/tpms/prefix
RUN /bin/bash -c "source ~/prefix/setup_env.sh && uhd_images_downloader"
USER root
RUN cp /home/tpms/prefix/src/uhd/host/utils/uhd-usrp.rules /etc/udev/rules.d/.



# Get ready in case a limesdr is used
WORKDIR /home/tpms/prefix/src/limesuite/udev-rules
RUN sudo chmod 775 install.sh
RUN sudo ./install.sh

# Get ready in case the plutosdr is used
USER root
WORKDIR /etc/udev/rules.d/
RUN wget https://raw.githubusercontent.com/analogdevicesinc/plutosdr-fw/master/scripts/53-adi-plutosdr-usb.rules
USER root
RUN ldconfig

# Make sure that the pybombs setup_env.sh is sourced in bash shells
USER tpms
RUN echo "source /home/tpms/prefix/setup_env.sh" >> /home/tpms/.bashrc

# Set up X11 configuration
USER root
RUN echo "X11UseLocalHost yes" >> /etc/ssh/sshd_config
RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config
RUN echo "Port 1022" >> /etc/ssh/sshd_config
RUN echo "AddressFamily any" >> /etc/ssh/sshd_config
RUN echo "ListenAddress 0.0.0.0" >> /etc/ssh/sshd_config
RUN echo "ListenAddress ::" >> /etc/ssh/sshd_config

#RUN udevadm control --reload-rules
#RUN udevadm trigger


USER root
EXPOSE 22
ENTRYPOINT service ssh restart && chown -R tpms:tpms /home/tpms/working && /bin/bash
